import argparse
import json
import os
from datetime import datetime

from src.models.classification import TickerSymbolRecognizer
from utils import RECOGNIZER_PATH, RESULT_DIR

parser = argparse.ArgumentParser()
parser.add_argument("--input", help="path to file with texts for ticker recognition")
parser.add_argument("--json", help="is input data in json file or not", default=0, type=bool)
parser.add_argument("--output", help="path to file for printing results",
                    default=os.path.join(RESULT_DIR,
                                         'result_dump_{date:%Y-%m-%d_%H:%M:%S}.json'.format(date=datetime.now())))
args = parser.parse_args()

if args.output.startswith(RESULT_DIR) and not os.path.exists(RESULT_DIR):
    os.mkdir(RESULT_DIR)

recognizer = TickerSymbolRecognizer.load(RECOGNIZER_PATH)
answers = []
with open(args.input, 'r') as file:
    if args.json:
        texts = json.load(file)
    else:
        texts = file.readlines()
    for text in texts:
        answers.append(recognizer.predict(text))

with open(args.output, 'w') as file:
    json.dump(answers, file)
