import argparse
import json

from src.models.classification import TickerSymbolRecognizer
from utils import TRAIN_TEXTS_PATH, RECOGNIZER_PATH

parser = argparse.ArgumentParser()
parser.add_argument("--input", help="path to file with texts for training", default=TRAIN_TEXTS_PATH)
parser.add_argument("--json", help="is input data in json file or not", default=0, type=bool)
args = parser.parse_args()

recognizer = TickerSymbolRecognizer()
with open(TRAIN_TEXTS_PATH, 'r') as file:
    if args.json:
        train_texts = json.load(file)
    else:
        train_texts = file.readlines()
recognizer.fit(train_texts, trainable_embeddings=False)
recognizer.save(RECOGNIZER_PATH)
