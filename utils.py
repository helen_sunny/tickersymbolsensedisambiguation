import os
from datetime import datetime

RAW_DATA_DIR = 'data/raw/'
TWEETS_WITH_TICKERS_PATH = os.path.join(RAW_DATA_DIR, 'tweets_with_tickers.csv')
TWEETS_WITHOUT_TICKERS_PATH = os.path.join(RAW_DATA_DIR, 'tweets_without_tickers.csv')
TWEETS_WITH_BOTH_PATH = os.path.join(RAW_DATA_DIR, 'tweets_with_both.csv')

PROCESSED_DATA_DIR = 'data/processed/'
TRAIN_TEXTS_PATH = os.path.join(PROCESSED_DATA_DIR, 'train_texts.json')
TEST_TEXTS_PATH = os.path.join(PROCESSED_DATA_DIR, 'test_texts.json')

MODELS_DIR = 'models/'
RECOGNIZER_PATH = os.path.join(MODELS_DIR, 'recognizer.model')
MODEL_WEIGHTS_PATH = os.path.join(MODELS_DIR, 'classifier_weights.hdf5')
EMBEDDING_MATRIX_PATH = os.path.join(MODELS_DIR, 'embedding_matrix.json')
FASTTEXT_MODEL_PATH = os.path.join(MODELS_DIR, 'wiki.en.bin')

TICKER_LIST_PATH = 'data/external/tickers.txt'
TICKERS = set(map(lambda x: x.strip(), open(TICKER_LIST_PATH, 'r').readlines()))

RESULT_DIR = 'results/'
