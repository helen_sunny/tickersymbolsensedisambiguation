import json
import re

import pandas as pd
from sklearn.model_selection import train_test_split

from utils import TWEETS_WITH_TICKERS_PATH, TWEETS_WITHOUT_TICKERS_PATH, TWEETS_WITH_BOTH_PATH, TRAIN_TEXTS_PATH, \
    TEST_TEXTS_PATH


def clean_text(text):
    """
    Remove hashtags, links to users and Web Links
    :param text: raw text
    :return: cleaned text
    """
    link_pattern = re.compile(r"https*://[^ ]*")
    person_pattern = re.compile(r"@[\w]+")
    hashtags_pattern = re.compile(r"([\$#][\w]+[|, ]*){4,}")
    text = re.sub(link_pattern, '', text)
    text = re.sub(person_pattern, '', text)
    text = re.sub(hashtags_pattern, '', text)
    return text


def clean_data_frames():
    """
    Clean text in all raw DataFrames, unite and remove duplicates
    :return: DataFrame
    """
    df1 = pd.read_csv(TWEETS_WITH_TICKERS_PATH, header=None).drop_duplicates(subset=[0])
    df2 = pd.read_csv(TWEETS_WITHOUT_TICKERS_PATH, header=None).drop_duplicates(subset=[0])
    df3 = pd.read_csv(TWEETS_WITH_BOTH_PATH, header=None).drop_duplicates(subset=[0])

    data = []
    for _, (id, _, ticker, text) in df1.iterrows():
        cleaned_text = clean_text(text.lower())
        if ticker in cleaned_text:
            data.append([id, cleaned_text, ticker])

    for _, (id, _, ticker, text) in df2.iterrows():
        cleaned_text = clean_text(text.lower())
        if ticker in cleaned_text:
            data.append([id, cleaned_text, ticker])

    for _, (id, _, ticker1, ticker2, text) in df3.iterrows():
        cleaned_text = clean_text(text.lower())
        if ticker1 in cleaned_text or ticker2 in cleaned_text:
            data.append([id, cleaned_text, ticker1])

    cleaned_data = pd.DataFrame(data, columns=['id', 'text', 'ticker']).drop_duplicates(subset=['id'])
    return cleaned_data


def split_data(df):
    """
    Split tweets to train and test sets and save
    :param df: data
    """
    stratify_array = df['ticker'].values
    train_texts, test_texts = train_test_split(df['text'].values, test_size=0.2, random_state=15,
                                               stratify=stratify_array)

    with open(TRAIN_TEXTS_PATH, 'w') as json_file:
        json.dump(list(train_texts), json_file)

    with open(TEST_TEXTS_PATH, 'w') as json_file:
        json.dump(list(test_texts), json_file)


if __name__ == "__main__":
    cleaned_data = clean_data_frames()
    split_data(cleaned_data)

