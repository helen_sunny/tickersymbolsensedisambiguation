import pandas as pd
from nltk import word_tokenize

from utils import TICKERS


def label_text(text):
    """
    Remove '$' symbol and label ticker symbols
    :param text: raw text
    :return: pairs of token and label
    """
    tokens = word_tokenize(text)
    labeled_tokens = list(map(set_label, ['#'] + tokens, tokens))
    filtered_tokens = [(token, label) for index, (token, label) in enumerate(labeled_tokens) if
                       index == len(labeled_tokens) - 1 or not labeled_tokens[index + 1][1]]
    return filtered_tokens


def set_label(prev_word, word):
    """
    Label ticker symbols
    :param prev_word: previous word
    :param word: word
    :return: pair of word and label
    """
    if prev_word == '$' and word in TICKERS:
        return word, True
    else:
        return word, False


def context_ticker_split(texts, context_size=15):
    """
    Split raw text on the ticker and its context
    :param texts: raw texts
    :param context_size: num words from each side
    :return: DataFrame with context, ticker and raw text
    """
    labeled_texts = []
    for text in texts:
        labeled_texts.append(label_text(text))
    data = []
    for labeled_text, text in zip(labeled_texts, texts):
        labeled_text = [('<s>', False)] * context_size + labeled_text + [('<e>', False)] * context_size
        index_token = [(i, token) for i, (token, label) in enumerate(labeled_text) if token in TICKERS]
        for index, token in index_token:
            sequence = [x[0] for x in labeled_text[index - context_size: index + context_size + 1]]
            label = labeled_text[index][1]
            sequence.pop(context_size)
            data.append([sequence, token, label, text])
    df = pd.DataFrame(data, columns=['context', 'ticker', 'target', 'raw_text'])
    return df
