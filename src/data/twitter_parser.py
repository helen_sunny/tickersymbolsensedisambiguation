import csv
import os
from itertools import permutations

import pandas as pd
import tweepy
from tqdm import tqdm

from utils import TICKER_LIST_PATH, TWEETS_WITH_TICKERS_PATH, TWEETS_WITHOUT_TICKERS_PATH, TWEETS_WITH_BOTH_PATH

consumer_key = 'Mcp0XCtfUnNbStxGlk5KC5O7H'
consumer_secret = 'vp69ZEDdHdVSpAVMsXBEizl78zXzRpzcTjlsXHTlItA3j31t06'
access_token = '709131903044100097-InNlqNWHZkYHSPtdTUtFdKV1IvHbna0'
access_token_secret = 'mp5Xm7B0N75sKNjZ2k4nOWKDx5RQCCfydgRArVqb0OUhG'

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth, wait_on_rate_limit=True)


def collect_tweets(path, queries, num_tweets=1000):
    """
    Find tweets by queries and save to file
    :param path: path to file
    :param queries: list of queries
    :param num_tweets: limit of found tweets
    """
    ids = get_existed_ids(path)
    csv_file = open(path, 'a')
    csv_writer = csv.writer(csv_file)
    for query in queries:
        for tweet in tqdm(tweepy.Cursor(api.search, q=query, count=10,
                                        lang="en",
                                        since="2018-06-01",
                                        tweet_mode="extended"
                                        ).items(num_tweets),
                          total=num_tweets, desc=query):

            if hasattr(tweet, 'retweeted_status'):
                tweet = tweet.retweeted_status
            id_str = tweet.id_str
            if id_str in ids:
                continue
            csv_writer.writerow([id_str, tweet.created_at, *query.split(' AND '), tweet.full_text])
            ids.add(id_str)


def get_existed_ids(path):
    """
    Return tweet ids which already downloaded
    :param path: path to DataFrame
    :return: set of existed ids
    """
    if os.path.exists(path):
        df = pd.read_csv(path, header=None)
        ids = set(df[0].values)
    else:
        ids = set()
    return ids


if __name__ == '__main__':
    lines = open(TICKER_LIST_PATH, 'r').readlines()
    words = list(map(lambda x: x.strip(), lines))
    tickers = map(lambda x: '${}'.format(x), words)
    pairs = map(lambda x, y: '${} AND {}'.format(x, y), *list(zip(*permutations(words, 2))))

    collect_tweets(TWEETS_WITH_TICKERS_PATH, tickers)
    collect_tweets(TWEETS_WITHOUT_TICKERS_PATH, words)
    collect_tweets(TWEETS_WITH_BOTH_PATH, pairs)
