from copy import copy

from gensim.models.wrappers import FastText
from keras import Sequential
from keras.layers import Embedding, Bidirectional, LSTM, Dropout, Dense, np
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.sequence import pad_sequences
from nltk import word_tokenize
from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix, classification_report

from src.data.make_dataset import context_ticker_split
from utils import FASTTEXT_MODEL_PATH, TICKERS, MODEL_WEIGHTS_PATH

EXCHANGE_NAMES = {'nasdaq', 'nyse', 'amex'}


def create_model(input_length, num_words, embedding_matrix=None):
    """
    Configure a Keras model
    :param input_length: length of input sequences
    :param num_words: size of the vocabulary
    :param embedding_matrix: word embeddings
    :return: Keras model
    """
    model = Sequential()
    model.add(Embedding(input_dim=num_words + 1,
                        output_dim=300,
                        input_length=input_length,
                        weights=embedding_matrix,
                        trainable=True
                        ))
    model.add(Bidirectional(LSTM(units=256, activation='sigmoid', recurrent_activation='hard_sigmoid')))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    return model


def collect_embedding_matrix(shape, word_index):
    """
    Collect FastText embeddings to matrix
    :param shape: matrix shape
    :param word_index: words with indices
    :return: embedding matrix
    """
    fasttext_model = FastText.load_fasttext_format(FASTTEXT_MODEL_PATH)
    embedding_matrix = np.zeros(shape)
    for word, i in word_index.items():
        try:
            embedding_vector = fasttext_model[word]
            embedding_matrix[i] = embedding_vector
        except KeyError as e:
            print(e)
    return [embedding_matrix]


class TickerSymbolRecognizer:
    def __init__(self):
        self.classifier = None
        self.tokenizer = None
        self.input_length = None
        self.num_words = None
        self.context_size = 15
        self.embedding_size = 300

    def fit(self, texts, trainable_embeddings=False):
        """
        Process raw data and fit Keras classifier
        :param texts: list of texts
        :param trainable_embeddings: use pre-trained embeddings or not
        """
        context_df = context_ticker_split(texts, context_size=self.context_size)

        self.tokenizer = Tokenizer()
        self.tokenizer.fit_on_texts(context_df['context'].values)
        word_index = self.tokenizer.word_index
        self.num_words = len(word_index)
        X = self.get_features(context_df['context'].values)
        self.input_length = len(X[0])
        y = context_df['target'].values.reshape(-1)

        if trainable_embeddings:
            embedding_matrix = collect_embedding_matrix(shape=(self.num_words + 1, self.embedding_size),
                                                        word_index=word_index)
        else:
            embedding_matrix = None

        self.classifier = create_model(input_length=self.input_length,
                                       num_words=self.num_words,
                                       embedding_matrix=embedding_matrix)
        self.classifier.fit(X, y, batch_size=64, epochs=3, validation_split=0.0, verbose=1, )

    def predict(self, text):
        """
        Find mention of any word in the given list and predict class (ticker symbol / not ticker symbol)
        :param text: raw text
        :return: pairs of word and label
        """
        tokens = word_tokenize(text.lower())
        result = []
        for index, token in enumerate(tokens):
            if token in TICKERS:
                if index != 0 and tokens[index - 1] == '$':
                    result.append((token, True))
                elif EXCHANGE_NAMES.intersection(set(tokens[index - 2:index])):
                    result.append((token, True))
                else:
                    context = copy(tokens)
                    context.pop(index)
                    context = ['<s>'] * self.context_size + context + ['<e>'] * self.context_size
                    context = context[index:index + 2 * self.context_size]
                    features = self.get_features([context])
                    label = bool(self.classifier.predict_classes(features)[0])
                    result.append((token, label))
        return result

    def evaluate(self, texts):
        """
        Check metrics on test data
        :param texts: test texts with '$' symbols
        :return: metrics
        """
        context_df = context_ticker_split(texts, context_size=self.context_size)
        X_test = self.get_features(context_df['context'].values)
        y_test = context_df['target'].values
        y_pred = self.classifier.predict_classes(X_test).reshape(-1)
        tn, fp, fn, tp = confusion_matrix(y_test, y_pred, labels=[True, False]).ravel()
        context_df['y_pred'] = y_pred
        print(classification_report(context_df['target'], context_df['y_pred']))
        return tn, fp, fn, tp

    def get_features(self, contexts):
        """
        Translate words into numbers
        :param contexts: list of context words
        :return: sequences
        """
        sequences = pad_sequences(self.tokenizer.texts_to_sequences(contexts))
        return sequences

    def save(self, path):
        """
        Save recognizer object
        :param path: model path
        """
        self.classifier.save_weights(MODEL_WEIGHTS_PATH)
        self.classifier = None
        joblib.dump(self, path)

    @classmethod
    def load(cls, path):
        """
        Load recognizer object
        :param path: model path
        :return: recognizer object
        """
        class_object = joblib.load(path)
        class_object.classifier = create_model(input_length=class_object.input_length, num_words=class_object.num_words)
        class_object.classifier.load_weights(MODEL_WEIGHTS_PATH)
        class_object.classifier._make_predict_function()
        return class_object
