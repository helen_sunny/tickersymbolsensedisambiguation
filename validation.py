import json

from src.models.classification import TickerSymbolRecognizer
from utils import RECOGNIZER_PATH, TEST_TEXTS_PATH

recognizer = TickerSymbolRecognizer.load(RECOGNIZER_PATH)
with open(TEST_TEXTS_PATH, 'r') as json_file:
    test_texts = json.load(json_file)
tn, fp, fn, tp = recognizer.evaluate(test_texts)
print('tn:', tn, 'fp:', fp, 'fn:', fn, 'tp:', tp)
